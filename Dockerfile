FROM openjdk:8-jdk

# ENV DEBIAN_FRONTEND noninteractive

ENV FASTLANE_VERSION=2.209.1
ENV ANDROID_COMPILE_SDK=31
ENV ANDROID_BUILD_TOOLS=32.0.0
ENV ANDROID_SDK_TOOLS=4333796
ENV FLUTTER_SDK=2.10.2

ENV ANDROID_HOME=/opt/android-sdk
ENV FLUTTER_HOME=/opt/flutter

WORKDIR /opt

RUN apt-get update && apt update && apt-get install -y wget unzip tar build-essential
RUN yes | apt-get install ruby-full

# download yq to download folder
RUN wget --quiet --output-document=yq https://github.com/mikefarah/yq/releases/download/v4.24.5/yq_linux_amd64

RUN chmod +x yq
ENV PATH=${PATH}:/opt/

# download android sdk tools 
RUN wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip

# unzip android sdk tools into opt file
RUN unzip android-sdk.zip -d /opt/android-sdk

# set path of the android tools 
ENV PATH=${PATH}:${ANDROID_HOME}/tools/bin

# Create Config Folder and File
RUN mkdir -p ~/.android && touch ~/.android/repositories.cfg

RUN yes | sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}" > /dev/null
RUN yes | sdkmanager "platform-tools" > /dev/null
RUN yes | sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}" > /dev/null
RUN yes | sdkmanager "cmdline-tools;latest" > /dev/null

RUN yes | sdkmanager --licenses

RUN gem install fastlane 

# #setup flutter sdk 
RUN wget --quiet --output-document=flutter-sdk.tar.xz https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_${FLUTTER_SDK}-stable.tar.xz
RUN tar -xf flutter-sdk.tar.xz 

# remove download zip file
RUN rm flutter-sdk.tar.xz && rm android-sdk.zip

ENV PATH=${PATH}:${FLUTTER_HOME}/bin

RUN yes | flutter doctor --android-licenses